package com.example.oskar.proyecto;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class login extends Activity {

    private EditText txtUsername;
    private EditText txtClave;
    TextView msjError;
    private Button login;
    private Button registrar;
    private Button regresar;

    // JSON parser class
    JSONParser jsonParser = new JSONParser();


    // url to update product
    //private static final String url_update_product = "http://10.0.2.2/android_connect/update_product.php";
    private static final String url_login_gasolinera = "http://54.200.253.54/gasolinera/login_gasolinera.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_USERNAME = "username";
    private static final String TAG_CLAVE = "clave";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        txtUsername = (EditText) findViewById(R.id.loginUsername);
        txtClave = (EditText)findViewById(R.id.loginClave);
        msjError = (TextView)findViewById(R.id.login_error);
        login = (Button)findViewById(R.id.btnLogin);
        registrar=(Button)findViewById(R.id.btnRegister);
        regresar=(Button)findViewById(R.id.btnHome);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // starting background task to update product
                new LoginCount().execute();
                msjError.setText(" Usuario o clave incorrecto ");
            }
        });

        // Te envia a que te registres
        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(login.this, registro.class);
                startActivity(i);
            }
        });

        // Regresas a Home
        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(login.this, MainActivity.class);
                startActivity(i);
            }
        });
    }

    /**
     * Background Async Task to  Save product Details
     * */
    class LoginCount extends AsyncTask<String, String, String> {

        protected String doInBackground(String... args) {

            // getting updated data from EditTexts
            String username = txtUsername.getText().toString();
            String clave = txtClave.getText().toString();

            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(TAG_USERNAME, username));
            System.out.println("imprimiendo username " + username);
            params.add(new BasicNameValuePair(TAG_CLAVE, clave));
            System.out.println("imprimiendo clave " + clave);

            // sending modified data through http request
            // Notice that update product url accepts POST method
            JSONObject json = jsonParser.makeHttpRequest(url_login_gasolinera,"POST", params);

            // check json success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    System.out.println("lo logre");
                    Intent i = new Intent(login.this, gasolinera.class);
                    startActivity(i);
                } else {
                    System.out.println("fallo");
                    // failed to update product
                    //msjError.setText(" Usuario o clave incorrecto ");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }


}
