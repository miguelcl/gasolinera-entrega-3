package com.example.oskar.proyecto;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class registro extends Activity {

    private EditText txtUsername;
    private EditText txtClave;
    private EditText txtNombre;
    public TextView msjError;
    private Button irLogin;
    private Button registrar;

    // JSON parser class
    JSONParser jsonParser = new JSONParser();


    // url to update product
    //private static final String url_update_product = "http://10.0.2.2/android_connect/update_product.php";
    private static final String url_create_gasolinera = "http://54.200.253.54/gasolinera/create_gasolinera.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_USERNAME = "username";
    private static final String TAG_CLAVE = "clave";
    private static final String TAG_NOMBRE = "nombre";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registro);
        txtUsername = (EditText) findViewById(R.id.registerUsername);
        txtClave = (EditText) findViewById(R.id.registerClave);
        txtNombre = (EditText) findViewById(R.id.registerNombre);
        msjError = (TextView)findViewById(R.id.registro_error);
        irLogin=(Button)findViewById(R.id.btntologin);
        registrar=(Button)findViewById(R.id.btnRegister);

        registrar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // starting background task to update product
                new CreateCount().execute();
                msjError.setText(" El usuario ya existe ");
                //Intent i = new Intent(registro.this, login.class);
                //startActivity(i);
            }
        });
        //////////////////////////////// Registro
        irLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(registro.this, login.class);
                startActivity(i);
            }
        });
    }


    /**
     * Background Async Task to  Save product Details
     * */
    class CreateCount extends AsyncTask<String, String, String> {

        protected String doInBackground(String... args) {

            // getting updated data from EditTexts
            String username = txtUsername.getText().toString();
            String clave = txtClave.getText().toString();
            String nombre = txtNombre.getText().toString();

            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(TAG_USERNAME, username));
            System.out.println("imprimiendo username"+username);
            params.add(new BasicNameValuePair(TAG_CLAVE, clave));
            System.out.println("imprimiendo clave"+clave);
            params.add(new BasicNameValuePair(TAG_NOMBRE, nombre));
            System.out.println("imprimiendo nombre"+nombre);

            // sending modified data through http request
            // Notice that update product url accepts POST method
            JSONObject json = jsonParser.makeHttpRequest(url_create_gasolinera,"POST", params);

            // check json success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // successfully updated
                    Intent i = getIntent();
                    // send result code 100 to notify about product update
                    setResult(100, i);
                    finish();
                } else {
                    // failed to update product
                    //msjError.setText("El usuario ya existe");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}

