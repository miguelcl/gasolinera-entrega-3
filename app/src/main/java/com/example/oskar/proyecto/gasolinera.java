package com.example.oskar.proyecto;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class gasolinera extends AppCompatActivity {
    private Button datos;
    private Button productos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gasolinera);
        datos = (Button)findViewById(R.id.btnDato);
        productos=(Button)findViewById(R.id.btnProducto);

        productos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(gasolinera.this, gasolinera_producto.class);
                startActivity(i);
            }
        });

    }
}

